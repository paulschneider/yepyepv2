function Carousel(config)
{
	var timer;
	var current;
	var target		= config.target;
	var totalItems 	= config.items.length;
	var thisItem 	= null;
	var continueOn  = true;

	this.init = function()
	{
		paginate(target, totalItems);

		$(config.items).css({
			'display' 	: 'none'
			,'opacity' 	: 0
			,'filter'	: 'alpha(opacity = 0)'
		});

		$(config.items).first().css({
			'display' 	: 'block'	
			,'opacity'	: 1
			,'filter'	: 'alpha(opacity = 100)'
		});

		timer = setTimeout(function(){
			transitionOut($(config.items).first(), false);	
		}, config.startDelay);		
	}

	var paginate = function(target, totalItems)
	{
		$(target).append('<ul class="pages" />')

		for(var i=1; i<=totalItems; i++)
		{
			$('.pages').append('<li class="page"><a href="#"></a></li>');
		}

		//$('.pages').append('<li id="continue"><img src="/_assets/img/continue-disabled.png" width="25" height="25" /></li>');

		$('.pages').css({
			position 	: 'absolute'
			,bottom 	: '-35px'
			,right 		: '0px'
		});

		$('.pages li').css({
			float 			: 'left'
			,width 			: '20px'
			,height 		: '20px'
			,'margin-left' 	: '8px'
			,cursor 		: 'pointer'
			,'list-style'	: 'none'
		});

		$('li.page').css('background', 'url(/_assets/img/page.png) no-repeat');

		$('li#continue').css({

			'margin-top' : '-2px'

		});

		$('.pages li:eq(0)').css({
			'background-position' : '-20px 0'
		});
	}

	var transitionOut = function(item, showFirst) 
	{		
		if(continueOn)
		{
			if(config.crossFade)
			{
				$(item).animate({
					opacity 	: 0
					,'filter'	: 'alpha(opacity = 0)'
				}, config.speed/2, function(){
					$(this).css('display', 'none');
				});

				timer = setTimeout(function(){
					if(!showFirst)
					{
						transitionIn($(item).next(), false);	
					}
					else
					{
						transitionIn($(config.items).first(), false);
					}
				}, (config.speed/100)*0.90);
			}
			else
			{
				$(item).animate({
					opacity 	: 0
					,'filter'	: 'alpha(opacity = 0)'
				}, config.speed, function() {
					$(this).css('display', 'none');

					if(!showFirst)
					{
						transitionIn($(item).next(), false);	
					}
					else
					{
						transitionIn($(config.items).first(), false);
					}	
				});
			}
		}
	}

	var transitionIn = function(item)
	{
		if(continueOn)
		{
			$(item).css('display', 'block');

			setTimeout(function(){
				setPage(item);
			}, 900);

			$(item).animate({
				opacity 	: 1
				,'filter'	: 'alpha(opacity = 100)'
			}, config.speed, function(){

				setTimeout(function(){
					if($(item).index()+1 == totalItems)
					{
						transitionOut($(item), true);
					}
					else
					{	
						transitionOut($(item), false);
					}
				}, config.waitTime);
			});
		}
	}

	var crossFade = function(item)
	{
		setTimeout(function(){
			transitionIn($(item).next());
		}, config.speed/2);
	}

	var setPage = function(item)
	{
		$('.pages li').css({
			'background-position' : '0 0'
		});

		$('.pages li:eq('+$(item).index()+')').css({
			'background-position' : '-20px 0'
		});
	}

	$(document).on('click', '.pages li.page', function(e){
		e.preventDefault();	

		continueOn 		= false;
		var thisIndex 	= $(this).index();

		$('#continue').find('img').attr('src', '/_assets/img/continue-disabled.png');

		$(config.items).animate({
			opacity 	: 0
			,'filter'	: 'alpha(opacity = 0)'
		}, 1000, function(){
			$(config.items).css('display', 'none');

			$(config.items).each(function(index, value){
				if(index == thisIndex)
				{
					current = $(this);

					setPage(this);

					$(this).css('display', 'block');
					
					$(this).animate({
						opacity 	: 1
						,'filter'	: 'alpha(opacity = 100)'
					}, 1000);
					return;		
				}
			});
		});
	});

	this.init();

	$('#continue').on('mouseover', this, function(e){

		if(!continueOn)
		{
			$(this).find('img').attr('src', '/_assets/img/repeat.gif');
		}

	}).mouseout(function(){

		if(!continueOn)
		{
			$(this).find('img').attr('src', '/_assets/img/continue-disabled.png');	
		}		

	});	

	$('#continue').on('click', this, function()
	{
		if(!continueOn)
		{
			continueOn = true;
			
			$(this).find('img').attr('src', '/_assets/img/continue-disabled.png');

			if($(current).index()+1 == totalItems)
			{
				transitionOut($(current), true);
			}
			else
			{	
				transitionOut($(current), false);
			}
		}
	});
}