$(document).ready(function(){

	$carousel = $('.carousel');

	var carousel = new Carousel({
		target 		: $carousel
		,items 		: $carousel.find('li')
		,startDelay	: 5000
		,speed 		: 5000
		,waitTime 	: 5000
		,crossFade 	: true
	});	

});