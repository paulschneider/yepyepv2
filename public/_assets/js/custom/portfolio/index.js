var activeClass 		= 'active';
var action 				= '';
var scaleVal 			= 10;
var curPage 			= 1;
var scrolls 			= 0;
var scrollTotal 		= 0;
var perPage 			= 5;
var totalItems 			= 0;
var selectedImgIndex;
var nextImgSrc;


$(document).ready(function(){

	/*
	* Carousel thumbnail click actions
	*/

	$('.carousel li').on('click', this, function(){

		if(!$(this).hasClass(activeClass))
		{
			$el 		= $(this);
			var newImg 	= $('img', $el).attr('src');

			$('.carousel li').removeClass(activeClass);			

			$el.addClass(activeClass);

			$('.preview img').animate({

				'opacity' : 0

			}, 550, function(){

				$(this).attr('src', newImg);

				$(this).animate({

					'opacity' : 1					

				}, 550);

			});

		}

	});

	/*
	* Preview image click action
	*/

	$('.preview img').on('click', this, function(){

		// Vars
		$el 				= $(this);		
		var img 			= $el.attr('src');


		$('.carousel ul li').each(function(index, value){

			if($('img', this).attr('src') == img)
			{

				selectedImgIndex = index;

			}

		});

		$('body').append('<div id="overlay"></div>');
		$('#content').append('<div id="preview"></div>');
		$('#preview').append('<img width="84" height="60" />'); // These dimension are small as they'll be transitioned by transit JS
		$('#preview img').attr('src', img);		

		$('#overlay').css({

			position 		: 'absolute'
			,top 			: 0
			,left 			: 0
			,width 			: $(document).width()
			,height 		: $(document).height()
			,background 	: '#000000'
			,opacity 		: 0
			,'z-index' 		: 10

		});

		$('#preview').css({

			position 					: 'absolute'
			,top 						: '300px'
			,left 						: '450px'
			,background 				: '#ffffff' 
			,border  					: '1px solid #13b5ea'
			,width 						: '84px'
			,height 					: '60px'
			,'z-index' 					: 12
			,overflow 					: 'hidden'

		});

		$('#preview img').css({
			
			opacity 					: 0

		});

		$('#overlay').animate({
			opacity : 0.7
		}, 300);

		setTimeout(function(){

			$('#preview').transition({ 

				scale 		: scaleVal 

			}, function(){

				$('#preview img').animate({
					
					opacity : 1

				}, 350);


				$(document).trigger('show_controls');				

			});

		}, 1000);

	});
	
	/*
	* Previous and Next carousel controls
	*/

	$('.carousel').on('click', '.cntrl', function(){

		var clicked;
		var width = 384;		

		if($(this).hasClass('nxt'))
		{	

			if(curPage * perPage <= $('.carousel ul li').length)
			{

				var left = parseInt($('.carousel ul').css('left'));

				if(left == 0)
				{
					newLeft = '-' + width + "px";
				}
				else
				{
					newLeft = left - width + 'px';
				}

				$('.carousel ul').animate({

					left : newLeft 

				}, 650);

				curPage++;

			}		

		}
		else
		{
			var newLeft = parseInt($('.carousel ul').css('left')) + width + 'px' ;

			if(parseInt(newLeft) <= 0)
			{
				$('.carousel ul').animate({

					left : newLeft 

				}, 650);

				curPage--;

			}

		}

	});


	/*
	* Add in controls to navigate the images while in preview mode
	*/

	$(document).on('show_controls', this, function(){

		$('#content').append('<div id="control"><span class="underlay"></span></div>');			
		$('#control').append('<div id="close"><a href="#">Close</a></div>');			
		$('#control').append('<div id="move"><a class="previous" href="#">Previous</a><a class="next" href="#">Next</a></div>');
		$('#control').append('<div id="count"><span class="from">1 </span> of <span class="total"></span></div>');
				
		$('#control').css({

			position 			: 'absolute'
			,top 				: '590px' 
			,left 				: '74px' 
			,height 			: '40px'
			,width 				: '838px' 
			,'z-index' 			: 50
			,opacity 			: 0

		});

		$('#control span.underlay').css({

			position 			: 'absolute'
			,top 				: '0' 
			,left 				: '0' 
			,height 			: '100%'
			,width 				: '100%'
			,'background' 		: '#000000' 
			,opacity 			: 0.7 

		});

		$('#close').css({

			position 			: 'absolute'
			,top 				: 0
			,left 				: 0
			,'z-index' 			: 55
			,'margin' 			: '12px 0 0 15px'

		});

		$('#move').css({

			position 			: 'absolute'
			,'z-index' 			: 55
			,'margin' 			: '12px 0 0 400px'

		});

		$('#move a, #close a').css({

			color 				: '#ffffff'
			,border 			: 'none' 
			,margin 			: '5px 10px 0 0'
			,cursor				: 'pointer'
			,padding 			: '5px'

		});

		$('#move a, #close a').hover(function()
		{

			$(this).css('background', '#13b5ea');


		}, function()
		{

			$(this).css('background', 'none');

		});


		$('#control:not(#control span)').animate({

			opacity 			: 1

		}, 650);

		$('#control #count').css({

			position 			: 'absolute'
			,bottom 			: '10px'
			,right 				: '20px'
			,color 				: '#ffffff'

		});

		// Update the counter at the bottom of the controls toolbar (e.g 1 of 5)
		$(document).trigger('update_count');

		// Initialise the controls toolbar (bottom of the preview lightbox)
		$(document).trigger('set_controls');

	});
	
	/*
	* Define the actions taken when the user interacts with the preview screen' controls
	*/

	$(document).on('set_controls', this, function(){

		$('#move').on('click', 'a.previous', function(e){

			e.preventDefault();

			var prevIndex = selectedImgIndex-1;

			if(prevIndex >= 0)
			{
				action = 'prev';

				nextImgSrc = $('.carousel li:eq('+prevIndex+') img').attr('src');				

				selectedImgIndex = prevIndex;

				$(document).trigger('update_image');

				$(document).trigger('update_carousel');
			}

		});

		$('#move').on('click', 'a.next', function(e){

			e.preventDefault();

			var nextIndex = selectedImgIndex+1

			if(nextIndex < $('.carousel li').length)
			{			
				action = 'next';

				nextImgSrc = $('.carousel li:eq('+nextIndex+') img').attr('src');				

				selectedImgIndex = nextIndex;

				$(document).trigger('update_image');

				$(document).trigger('update_carousel');				

			}
			
		});

	});

	/*
	* Alter the image based on the action taken within the preview screen' controls
	*/

	$(document).on('update_image', this, function(){

		$('#preview img, .preview img').animate({

			opacity : 0

		}, 550, function(){

			$('#preview img, .preview img').attr('src', nextImgSrc);

			$('#preview img, .preview img').animate({

				opacity : 1

			}, 550);

			$(document).trigger('update_count');

		});

	});

	/*
	* Update the counter at the bottom of the preview screen
	*/

	$(document).on('update_count', this, function(){

		// Set the image number for the chosen item within the toolbar of the preview lightbox (e.g 2 of ...)
		$('#count .from').text(selectedImgIndex+1);

		// Set the count of images for this image within the toolbar of the preview lightbox (e.g ... of 5)
		$('#count .total').text($('.carousel ul li').length);

	});

	/*
	* Update the carousel selection indicator to reflect the change in the chosen image from the preview screen
	*/

	$(document).on('update_carousel', this, function(){

		$('.carousel li').removeClass(activeClass);

		$('.carousel li:eq(' + selectedImgIndex + ')').addClass(activeClass);

		if( action == 'next' )
		{

			if( Math.ceil( selectedImgIndex / perPage ) > curPage )
			{

				$('.cntrl.nxt').trigger('click');

			}	

		}
		
		if( action == 'prev' )
		{
			if ( Math.ceil( selectedImgIndex / perPage ) < curPage )
			{

				$('.cntrl.pre').trigger('click');

			}
		}		

	});

	/*
	* Actions on clicking the opaque overlay
	*/

	$(document).on('click', '#overlay, #close a', function(e){

		e.preventDefault();

		$('#control').children().animate({

			opacity : 0

		}, 200);


		$('#preview').animate({

			opacity : 0

		}, 200, function(){

			$('#preview, #control').remove();			

			$('#overlay').animate({

				opacity : 0

			}, 200, function(){

				$('#overlay').remove();

			});

		});

	});

}(jQuery));