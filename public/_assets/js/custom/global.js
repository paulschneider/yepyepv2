var formVars = {};

$(document).ready(function(){

	$('header nav a').mouseover(function(){

		$(this).prev().css({

			color : '#162732'

		});	
	

	}).mouseout(function(){

		$(this).prev().css({

			color : '#ffffff'

		});

	});

	$('footer nav a').mouseover(function(){

		$(this).prev().css({

			color : '#13b5ea'

		});	
	

	}).mouseout(function(){

		$(this).prev().css({

			color : '#ffffff'

		});

	});

	/*
	* Form validation
	*/

	$('form[name=frm_contact]').validate(
	{
	    debug   	: true
	    ,highlight 	: function(element, errorClass, validClass)
	    {   	

	    	var errors = this.errorList;
     
			$(element).addClass('error');   

	    }
	    ,unhighlight : function(element, errorClass, validClass)
	    {
	        $(element).removeClass('error');
	    }
	    ,submitHandler: function(form) 
	    { 	

	        // Get all the form variables
	        formVars.name          		= $('input[name=name]').val();
	        formVars.email              = $('input[name=email]').val();
	        formVars.tel      			= $('input[name=tel]').val();
	        formVars.message  			= $('textarea[name=message]').val();

           	send();    

	    }
	    ,errorPlacement: function(error, element) 
	    {
	        // Do nothing
	    }
	    ,rules  : {
	        name : {
	            maxlength       : 50
	            ,required       : true

	        }
	        ,email : {
	            maxlength       : 100
	            ,required       : true
	            ,email          : true
	        }
	        ,tel : {
	            maxlength       : 20
	            ,required       : true
	        }
	        ,message : {
	            maxlength       : 500
	            ,minlength      : 1
	            ,required       : true
	        }          
	    }
	});

	/*
	* Submit the now validated form values to the server
	*/ 
	send = function()
	{
	    $.ajax({
	        url        	: '/ajax/form_submit'
	        ,type      	: 'POST'
	        ,data      	: formVars
	        ,dataType 	: 'json'
	        ,success   	: function(response)
	        {

	        	console.log(response);

	            $('footer p.msg').html(response.message)

				$('footer p.msg').css({

					display : 'block'

				})

				if( response.success )
				{

					$('footer p.msg').addClass('success');			

				}
				else
				{

					$('footer p.msg').addClass('error');

				}
	        }
	    });
	}

});