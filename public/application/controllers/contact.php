<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
	}	

	public function index()
	{	
		$this->page = 'contact/index';

		parent::render();
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */