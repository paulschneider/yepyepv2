<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Who extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
	}	

	public function index()
	{	
		$this->page = 'who/index';

		parent::render();
	}

	public function download($name="")
	{
		$aCv = $this->config->item('cvs');

		if(in_array($name, $aCv))
		{
			$this->load->helper('download');

			$file = file_get_contents('_assets/data/'.$name.'.pdf');

			$name = $name.'.pdf';

			force_download($name, $file);
		}
		else
		{
			redirect(base_url());
		}
	}
}

/* End of file who.php */
/* Location: ./application/controllers/who.php */