<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('contact_model');
		$this->load->library('email');
	}	

	public function form_submit()
	{
		if($this->input->post())
		{
			$params = $this->input->post();

			$result = $this->contact_model->save($params);

			if( isset($result['success']) )
			{
				$name 		= $params['name'];
				$email 		= $params['email'];
				$telephone 	= $params['tel'];
				$body 		= $params['message'];

				$message 	= 	"This is a new website enquiry:"."\r\n".
								"Name: ".$name."\r\n".
								"Email: ".$email."\r\n".
								"Telephone: ".$telephone."\r\n".				
								"Comment: ". $body;
				
		    	$this->email->set_newline("\r\n");	    	
		    	$this->email->from('info@yepyep.co.uk', 'Yep!Yep!');
		    	$this->email->to('info@yepyep.co.uk');
		    	$this->email->subject('[Enquiry] Yep!Yep!');
		    	$this->email->message($message);
		    	
		    	if($this->email->send())
		    	{
		    		$response = array('success' => true, 'message' => 'The message has been both saved and sent! Hussah!!');
		    	}
		    	else
		    	{
		    		$response = array('error' => true, 'message' => 'There was a problem emailing the contact enquiry. But its in the database!');
		    	}
			}
		}
		else
		{
			$response = array('error' => true, 'message' => 'No POST parameter provided');
		}		

		if( ENVIRONMENT == 'production' )
		{
			if( isset($response['success']) )
			{
				$response = array('success' => true, 'message' => 'Your enquiry has been successfully sent.');
			}
			else
			{
				$response = array('error' => true, 'message' => 'There was a problem sending your enquiry. Please try again. If the problem persists please email info@yepyep.co.uk.');
			}
		}

		print_r(json_encode($response));
	}
}

/* End of file who.php */
/* Location: ./application/controllers/who.php */