<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
	}	

	public function index()
	{	
		$this->data['js'][] = 'custom/carousel';
		$this->data['js'][] = 'custom/home/index';

		$this->page = 'home/index';

		parent::render();
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */