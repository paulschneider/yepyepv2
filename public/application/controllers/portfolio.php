<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Portfolio extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('portfolio_model');
		$this->load->helper('image_helper');
	}	

	public function index()
	{	
		$this->page = 'portfolio/index';

		parent::render();
	}

	public function detail($name="")
	{
		if(!empty($name))
		{
			$this->data['js'][] = 'custom/portfolio/index';

			$this->data['content'] = $this->portfolio_model->get($name);

			$this->data['images'] = get_images($name);

			$this->page = 'portfolio/detail';

			parent::render();	
		}
		else
		{
			redirect(base_url().'portfolio');
		}		
	}
}

/* End of file who.php */
/* Location: ./application/controllers/who.php */