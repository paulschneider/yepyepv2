<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Contact_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();		
	}

	public function save($post=array())
	{
		if( count($post) > 0 )
		{
			$params = array(

				'name' 			=> $post['name']
				,'email' 		=> $post['email']
				,'tel' 			=> $post['tel']
				,'message' 		=> $post['message']
				,'created'		=> date('Y-m-d H:i:s')

			);

			$this->db->insert('contact', $params);

			$id = $this->db->insert_id();

			if(!is_null($id))
			{
				$response = array('success' => true, 'message' => 'Contact record saved');
			}
			else
			{
				$response = array('error' => true, 'message' => 'There was a problem inserting the contact record into the database');		
			}
		}
		else
		{
			$response = array('error' => true, 'message' => 'Insufficient parameters to complete contact form save');
		}

		return $response;
	}

}

/* End of file portfolio_model.php */
/* Location: ./application/models/portfolio_model.php */