<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Portfolio_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();		
	}

	public function get($name="")
	{
		if(!empty($name))
		{
			$this->db->select('p.id, p.title, p.sef_name, p.project_date, p.overview, p.content')
					 ->from('portfolio p')					 
					 ->where('p.sef_name', $name);

			$query = $this->db->get();

			if($query->num_rows() > 0)
			{
				$result = $query->result();

				$result[0]->technology = array();

				$this->db->select('t.name')
						 ->from('portfolio_tech pt')
						 ->join('technology t', 't.id=pt.technology_id')
						 ->where('pt.portfolio_id', $result[0]->id);						 

				$query2 = $this->db->get();

				if($query2->num_rows() > 0)
				{
					$result2 = $query2->result_array();

					foreach($result2 AS $tech)
					{
						$result[0]->technology[] = $tech['name'];
					}
				}

				return $result[0];
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

}

/* End of file portfolio_model.php */
/* Location: ./application/models/portfolio_model.php */