<?php

/** 
* MY_Controller.php
*
* All sub controllers direct through this before the site is loaded
* @author : Paul Schneider <paul.schneider@yepyep.co.uk>
* 13 Nov 2013 @ 11:45
*/

Class MY_Controller extends CI_Controller
{
	/**
	*	
	* array $page
	*/	
	var $page;

	/**
	*	
	* array $data
	*/
	var $data;

	/**
	*	
	* string $pageTitle
	*/
	var $pageTitle='';

	/**
	*	
	* object $user
	*/
	var $user;

	/**
	*	
	* Set system defaults
	* @param null
	* @return null
	*
	*/
	public function __construct()
	{
		parent::__construct();

		$this->data['js']       	= array(
			'jquery/jquery-1.10.2.min'
			,'thirdparty/transit-0.9.9.min'	
			,'thirdparty/jquery.validate.min'	
			,'custom/global'
		);

		$this->data['css']      	= array(
		);  	
		
		$this->data['bodyClass']	= '';

		// Initiate the session if none exists
		if(session_id() == "")
		{
			session_start();	
		}
	}

	/**
	*	
	* Render the page by loading view requests sent from child pages
	* @param null
	* @return null
	*
	*/	
	public function render()
	{
		// Set the title of the page. Set in child views
		$this->data['pageTitle'] = $this->pageTitle;

		// Grab the global data array item and pass it to each of the view for use
		$data = $this->data;    

		// Load the main header files containing the declaration of the document
		$this->load->view('_incs/header', $data);

		// Load any pages passed from sub controllers. There might be several which will be in an array
		if(is_array($this->page)) 
		{
			foreach($this->page AS $view)
			{
				$this->load->view($view, $data);
			}
		}

		// Or there might be just one 
		else
		{
			$this->load->view($this->page, $data);
		}

		// Load the footer file containing JS includes and document closures 
		$this->load->view('_incs/footer', $data);
	}
}