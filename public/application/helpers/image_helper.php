<?php

function get_images($sef_name)
{

	$images = array();

	$filepath = '_assets/img/portfolio/' . $sef_name . '/slides';

	$excluded = array('DS_Store', '.', '..');

	if( is_dir($filepath) )
	{
		if( $handle = opendir($filepath) ) 
		{    	
	    	while( false !== ($file = readdir($handle)) ) 
	    	{
	    		if( !in_array($file, $excluded) )
	    		{
	    			$images[] = $filepath.'/'.$file;
	    		}
	    	}	    

	    	closedir($handle);
		}
	}
	
	return $images;
}