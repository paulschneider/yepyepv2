		<footer id="bottom">
			
			<section id="left">

				<h2>Send us a message</h2>

				<form method="post" name="frm_contact">

					<ul  class="lhs">
						<li>
							<input type="text" id="name" name="name" placeholder="Full name" />
						</li>
						<li>
							<input type="text" id="email" name="email" placeholder="Email address" />
						</li>
						<li>
							<input type="text" id="tel" name="tel" placeholder="Telephone number" />
						</li>
					</ul>
					<ul  class="rhs">
						<li>
							<textarea name="message" id="message" placeholder="Your message"></textarea>
						</li>
						<li class="sbm">
							<button class="btn">Send</button>
						</li>
					</ul>

					<p class="msg"></p>

				</form>

			</section>

			<section id="right">

				<h2>Where to?</h2>

				<nav>
					<ul>
						<li><span class="ico home"></span><a href="<?php echo base_url() ?>">Home</a></li>
						<li><span class="ico who"></span><a href="<?php echo base_url() ?>who-are-we">Who are we?</a></li>
						<li><span class="ico portfolio"></span><a href="<?php echo base_url() ?>portfolio">Portfolio</a></li>
						<li><span class="ico contact"></span><a href="<?php echo base_url() ?>contact">Get in touch</a></li>
					</ul>
				</nav>

			</section>

			<p class="copyright">&copy; <?php echo date('Y') ?> Yep! Yep! Ltd.</p>

		</footer>

	</div>

</body>

<?php foreach($js AS $j) : ?>
	<script type="text/javascript" src="<?php echo base_url() ?>_assets/js/<?php echo $j ?>.js"></script>
<?php endforeach ?>

<?php if(ENVIRONMENT == 'production') : ?>
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-35753845-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
<?php endif ?>

</html>