<!DOCTYPE HTML>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="Yep!Yep! are a London based web development company established in 2012." />
    <meta name="keywords" content="Yep!Yep, web development, web applications, London, websites, applications, freelance" />		
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Language" content="EN" />
	<meta name="Copyright" CONTENT="" />
	<meta name="ROBOTS" CONTENT="">

	<title>Yep!Yep!</title>

	<!-- CSS -->
	<link rel="stylesheet/less" type="text/css" href="<?php echo base_url() ?>_assets/css/boilerplate.css" />
	<link rel="stylesheet/less" type="text/css" href="<?php echo base_url() ?>_assets/css/fonts.css" />
	<link rel="stylesheet/less" type="text/css" href="<?php echo base_url() ?>_assets/css/content.less" />	

	<link rel="Shortcut icon" href="<?php echo base_url() ?>favicon.ico" />
	<link rel="icon" href="<?php echo base_url() ?>favicon.ico" type="image/x-icon" />		

	<!-- Javascript -->

	<script type="text/javascript" src="<?php echo base_url() ?>_assets/js/thirdparty/modernizr-2.6.3.min.js"></script>


	<script type="text/javascript">
	    less = {
	        env 				: "production" 	// "development"or "production"
	        ,async 				: false       	// load imports async
	        ,fileAsync 			: false   		// load imports async when in a page under
	                            				// a file protocol
	        ,poll 				: 1000         	// when in watch mode, time in ms between polls
	        ,functions 			: {}      		// user functions, keyed by name
	        ,dumpLineNumbers 	: "comments" 	// or "mediaQuery" or "all"
	        ,relativeUrls 		: false 		// whether to adjust url's to be relative
	                            				// if false, url's are already relative to the
	                            				// entry less file
	        ,rootpath 			: ":/a.com/"	// a path to add on to the start of every url
	                            				// resource
	    };
	</script>

	<script type="text/javascript" src="<?php echo base_url() ?>_assets/js/thirdparty/less.css-1.5.0.min.js"></script>

</head>

<body>

	<div id="content">

		<header id="top">

			<span class="banner left"></span>

			<div>
				<a href="<?php echo base_url() ?>">
					<img id="l-circle" src="<?php echo base_url() ?>_assets/img/logo.png" width="80" height="79" alt="Yep! Yep! Ltd logo" />			
					<img id="l-text" src="<?php echo base_url() ?>_assets/img/logo-text.png" width="123" height="25" alt="Yep! Yep! Ltd logo text" />			
				</a>

				<nav>
					<ul>
						<li><span class="ico home"></span><a href="<?php echo base_url() ?>">Home</a></li>
						<li><span class="ico who"></span><a href="<?php echo base_url() ?>who-are-we">Who are we?</a></li>
						<li><span class="ico portfolio"></span><a href="<?php echo base_url() ?>portfolio">Portfolio</a></li>
						<li><span class="ico contact"></span><a href="<?php echo base_url() ?>contact">Get in touch</a></li>
					</ul>
				</nav>
			</div>
			
			<span class="banner right"></span>

		</header>
		
		