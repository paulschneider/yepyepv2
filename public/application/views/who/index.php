<div class="page who">


	<div class="intro">

		<h2>Who are we?</h2>

		<p>
			Yep!Yep! is a London based web development company formed in September 2012. We are a full service company that is able to offer a multitude of services based on our experiences 
			on a number of varied and technically challenging projects including e-commerce websites, Facebook Applications, interactive HTML5 games and cloud based video streaming services. 		
		</p>

	</div>

	<div class="module left">

		<h3><a name="paul-schneider">Paul Schneider</a></h3>

		<div class="lhs">

			<img src="<?php echo base_url() ?>_assets/img/who/paul.jpg" width="220" height="179" alt="Paul Schneider profile image" />

		</div>

		<div class="rhs">

			<p>
				Paul is a founding director of Yep!Yep! and has over six years experience building web applications in a number of technologies and 
				environments. Paul specialises in backend systems built using PHP as well as frontend interface implementation using HTML 4&amp;5, CSS 2&amp;3, 
				OO Javascript and JQuery.
			</p>

			<p>
				Paul has served in a number of roles throughout his career including Front and Back end Developer, Technical Lead, Web Development Team Manager and Technical
				Architect.
			</p>

			<p>
				Paul has a first class degree in Business Information Technology from Bournemouth University and is currently working as a freelance web developer 
				in London. 
			</p>

			<p>
				For a PDF copy of his CV please click <a href="<?php echo base_url() ?>who-are-we/paul-schneider/cv">here</a>.
			</p>

			<p>
				You can check out Paul's Linked in profile at <a href="http://uk.linkedin.com/pub/paul-schneider/23/630/80a" target="_blank">http://uk.linkedin.com/pub/paul-schneider/23/630/80a</a>.
			</p>

		</div>

	</div>

</div>