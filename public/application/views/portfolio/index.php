<div class="page portfolio">

	<section id="top">
		
		<div class="intro">
			
			<h2>Our Portfolio</h2>

			<p>
				On this page you will find a list of the work we have carried out in our Freelance and Commercial Projects. Clicking into a 
				portfolio list item will let you read a little more about that project. The projects listed under each heading are chronologically ordered with the most recent appearing first.
			</p>

		</div>

	</section>

	<section id="freelance" class="projects">

		<h2>Freelance</h2>	

		<div class="item">
			<div class="mask"></div>
			<img src="<?php echo base_url() ?>_assets/img/portfolio/citroen-ds3-cabrio/thumb.jpg" width="113" height="113" alt="Citroen Card Game" />
			<span></span>
			<a href="<?php echo base_url() ?>portfolio/detail/citroen-ds3-cabrio">
				<div class="iContent">			
					<h3>DS3 Cabrio <br />Card <br />Trick</h3>
				</div>
			</a>
		</div>

		<div class="item">
			<div class="mask"></div>
			<img src="<?php echo base_url() ?>_assets/img/portfolio/omega-watches-cms/thumb.jpg" width="113" height="113" alt="Omega Watches CMS log in screen" />
			<span></span>
			<a href="<?php echo base_url() ?>portfolio/detail/omega-watches-cms">
				<div class="iContent">			
					<h3>Omega <br />Watches <br />CMS</h3>
				</div>
			</a>
		</div>

		<div class="item">
			<div class="mask"></div>
			<img src="<?php echo base_url() ?>_assets/img/portfolio/get-living-london/thumb.jpg" width="113" height="113" alt="Omega CMS Dashboard" />
			<span></span>
			<a href="<?php echo base_url() ?>portfolio/detail/get-living-london">
				<div class="iContent">			
					<h3>Get <br />Living <br />London</h3>
				</div>
			</a>
		</div>

		<div class="item">
			<div class="mask"></div>
			<img src="<?php echo base_url() ?>_assets/img/portfolio/body-of-a-champion/thumb.jpg" width="113" height="113" alt="Body of a champion question 1" />
			<span></span>
			<a href="<?php echo base_url() ?>portfolio">
				<div class="iContent">			
					<h3>Body of <br />a <br />Champion</h3>
				</div>
			</a>
		</div>

		<div class="item">
			<div class="mask"></div>
			<img src="<?php echo base_url() ?>_assets/img/portfolio/london-dogs-trust/thumb.jpg" width="113" height="113" alt="London Dogs Trust homepage" />
			<span></span>
			<a href="<?php echo base_url() ?>portfolio">
				<div class="iContent">			
					<h3>London <br />Dogs <br />Trust</h3>
				</div>
			</a>
		</div>


	</section>

	<section id="private" class="projects">

		<h2>Commercial Projects</h2>

		<div class="item">
			<div class="mask"></div>
			<img src="<?php echo base_url() ?>_assets/img/portfolio/marcel-fine-jewellery/thumb.jpg" width="113" height="113" alt="Marcel Fine Jewellery product listing" />
			<span></span>
			<a href="<?php echo base_url() ?>portfolio/detail/marcel-fine-jewellery">
				<div class="iContent">			
					<h3>Marcel <br />Fine <br />Jewellery</h3>
				</div>
			</a>
		</div>

		<div class="item">
			<div class="mask"></div>
			<img src="<?php echo base_url() ?>_assets/img/portfolio/collective-noun/thumb.jpg" width="113" height="113" alt="Collective noun homepage" />
			<span></span>
			<a href="<?php echo base_url() ?>portfolio">
				<div class="iContent">			
					<h3>Collective <br />Noun <br />Website</h3>
				</div>
			</a>
		</div>

		<div class="item">
			<div class="mask"></div>
			<img src="<?php echo base_url() ?>_assets/img/portfolio/open-for-business/thumb.jpg" width="113" height="113" alt="Open For Business homepage" />
			<span></span>
			<a href="<?php echo base_url() ?>portfolio">
				<div class="iContent">			
					<h3>Open <br />For <br />Business</h3>
				</div>
			</a>
		</div>

	</section>

</div>