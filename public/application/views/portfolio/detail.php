
<!-- Slide sizes

	Large - 840x600
	Medium - 420x300
	Small - 70x50

-->

<div class="page portfolio-detail">

	<section id="top">

		<h2><?php echo $content->title ?></h2>

	</section>

	<section id="overview">

		<div class="lhs">

			<div class="preview">
				
				<div>
					<img src="<?php echo base_url() ?>_assets/img/portfolio/<?php echo $content->sef_name ?>/slides/1.jpg" width="420" height="300" title="Click to zoom" />
				</div>

			</div>

			<div class="carousel">
				
				<span class="cntrl pre">
					<span class="icon"></span>
				</span>

				<div>
					<ul>

						<?php $s=1; foreach( $images AS $key => $image ) : ?>

							<?php 
								$class = $key == 0 ? 'class="active"' : '' ; // Set the very first item as the active one
								$class = $s == 5 ? 'class="last"' 	: $class; // If we get to the fifth item of a page set a class
							?>
								
							<li <?php echo $class ?>><img src="<?php echo base_url().$image ?>" width="70" height="50" /></li>

						<?php 							

							// If we have got to the last item on a page then reset the counter
							if($s == 5)
							{
								$s = 0;
							}

							// Increment the counter
							$s++;	

							endforeach;
						 ?>

					</ul>
				</div>

				<span class="cntrl nxt">
					<span class="icon"></span>
				</span>

			</div>

		</div>

		<div class="rhs">

			<h2>Overview</h2>

			<?php echo $content->overview ?>

			<h2>Technology</h2>

			<div class="tech">

				<ul>
					<?php foreach( $content->technology AS $tech ) : ?>

						<li><span><?php echo $tech ?></span></li>

					<?php endforeach ?>

				</ul>

			</div>

		</div>

	</section>

	<section id="description">

		<h2>Description</h2>

		<?php echo $content->content ?>

	</section>

</div>