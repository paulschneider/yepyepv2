<div class="page home">

	<div class="intro">
		<p>
			Hello!
		</p>
		<p>
			We are a small London based web application development company with big ideas. Take 
			a look around and see if there's anything we might be able to help your company with, if so <a href="<?php echo base_url() ?>contact">get in touch</a>.
		</p>
	</div>

	<h2>Some of our latest work!</h2>

	<div class="intro">
		<p>
			Here's a small selection of the most recent developments Yep!Yep! has been involved in. If you want to learn more you can read full
			project descriptions in our <a href="<?php echo base_url() ?>portfolio">portfolio</a> section.

		</p>
	</div>

	<?php $this->load->view('home/carousel') ?>

	<h2>What we do!</h2>

	<div class="intro">
		<p>Still wondering what we do? Hover over each of the images below to learn a little more of what we are about.</p>
	</div>

	<div class="focus">

		<div class="item">			
			<div class="mask"></div>
			<img src="<?php echo base_url() ?>_assets/img/what-we-do/omega-cms.jpg" width="235" height="235" alt="Omega CMS Dashboard" />
			<span></span>
			<div class="iContent">			
				<h3>We are <br />Freelancers!</h3>
				<p>First and foremost we are freelance web developers specialising in PHP development as well as HTML5, CSS3 <br />and JQuery.</p>
			</div>
		</div>

		<div class="item">
			<div class="mask"></div>
			<img src="<?php echo base_url() ?>_assets/img/what-we-do/mfj.jpg" width="235" height="235" alt="Marcel Fine Jewellery homepage" />
			<span></span>
			<div class="iContent">			
				<h3>Full Cycle Development</h3>
				<p>We are also an independent development company that can design, construct and maintain your website from beginning to end.</p>
			</div>
		</div>

		<div class="item">			
			<div class="mask"></div>
			<img src="<?php echo base_url() ?>_assets/img/what-we-do/citroen.jpg" width="235" height="235" alt="Citroen DS3 Card game" />
			<span></span>
			<div class="iContent">			
				<h3>What <br />we've done</h3>
				<p>We believe our work speaks for itself. Check out our <a href="<?php echo base_url() ?>portfolio">portfolio</a> to see if we can help you!</p>
			</div>
		</div>

		<div class="item">			
			<div class="mask"></div>
			<img src="<?php echo base_url() ?>_assets/img/what-we-do/beach.jpg" width="235" height="235" alt="Phu Quoc" />
			<span></span>
			<div class="iContent">			
				<h3>What we've <br /> been up to</h3>
				<p>Like everyone else we like to unwind. See where we've been recently in our social area.</p>
			</div>
		</div>

	</div>

</div>	