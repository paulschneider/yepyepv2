<div id='top' class='carousel'>
	<ul id='items'>
		<li class='first left'>
			<div class="dets">
				<h3>Citroen - Facebook App</h3>
				<p>
					Our most recent freelance development was a Facebook Canvas application for a Citroen/Arsenal Partnership consisting of a card trick
					competition in which the winner walked away with a <br />Citreon DS3 Cabrio!
				</p>
				<a  class="btn" href='<?php echo base_url() ?>portfolio/detail/citroen-ds3-cabrio'>Read more</a>
			</div>
			<div class='bg' style='background:url(<?php echo base_url() ?>_assets/img/_banners/fb-app.jpg) no-repeat'></div>
		</li>
		<li class='right'>
			<div class="dets">
				<h3>Omega Watches - CMS</h3>
				<p>
					Earlier this year Yep!Yep! was tasked with constructing a bespoke system to manage content for Omega's new YouTube brand channel.
				</p>
				<a  class="btn" href='<?php echo base_url() ?>portfolio/detail/omega-watches-cms'>Read more</a>
			</div>
			<div class='bg' style='background:url(<?php echo base_url() ?>_assets/img/_banners/omega-cms.jpg) no-repeat'></div>
		</li>
		<li class='left'>
			<div class="dets">
				<h3>Marcel Fine Jewellery</h3>
				<p>
					An on-going project, this was a full life-cycle build for a private client based in Clerkenwell, Central London consisting of an e-commerce site and bespoke CMS for an online jewellery store. 
				</p>
				<a  class="btn" href='<?php echo base_url() ?>portfolio/detail/marcel-fine-jewellery'>Read more</a>
			</div>
			<div class='bg' style='background:url(<?php echo base_url() ?>_assets/img/_banners/mfj.jpg) no-repeat'></div>
		</li>
	</ul>
</div>