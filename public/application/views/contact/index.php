<div class="page contact">

	<h2>Contact Us</h2>


	<div class="intro">

		<p>
			If you want to get in touch with us you can do so in a number of ways. We don't believe in telephones so take a look below and select a 
			contact option most suitable for your enquiry.
		</p>

		<p>
			Alternatively you can send us a message directly by using the contact form at the bottom of each page of the site.
		</p>

	</div>

	<section id="general">
		<h3>General</h3>
		<p>For general enquiries about Yep!Yep! or its work.</p>
		<a href="mailto:info@yepyep.co.uk">info@yepyep.co.uk</a>
	</section>

	<section id="accounts" class="last">
		<h3>Invoice</h3>
		<p>For enquiries about Yep!Yep! invoices.</p>
		<a href="mailto:accounts@yepyep.co.uk">accounts@yepyep.co.uk</a>
	</section>

	<section id="recruitment">
		<h3>Recruitment</h3>
		<p>To hire Yep!Yep! or its staff for your project.</p>
		<a href="mailto:cv@yepyep.co.uk">cv@yepyep.co.uk</a>
	</section>


</div>